Road Core 1.0.0
=====================

Intro
-----
Road is a free open source peer-to-peer electronic cash system that is
completely decentralized, without the need for a central server or trusted
parties.  Users hold the crypto keys to their own money and transact directly
with each other, with the help of a P2P network to check for double-spending.


Setup
-----
Unpack the files into a directory and run road-qt.exe.

Road Core is the original Road client and it builds the backbone of the network.
However, it downloads and stores the entire history of Road transactions;
depending on the speed of your computer and network connection, the synchronization
process can take anywhere from a few hours to a day or more.

See the road wiki at:
  https://roadcoin.atlassian.net/wiki/
for more help and information.
