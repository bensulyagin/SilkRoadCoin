
Debian
====================
This directory contains files used to package roadd/road-qt
for Debian-based Linux systems. If you compile roadd/road-qt yourself, there are some useful files here.

## road: URI support ##


road-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install road-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your road-qt binary to `/usr/bin`
and the `../../share/pixmaps/road128.png` to `/usr/share/pixmaps`

road-qt.protocol (KDE)

