Sample configuration files for:

SystemD: roadd.service
Upstart: roadd.conf
OpenRC:  roadd.openrc
         roadd.openrcconf
CentOS:  roadd.init
OS X:    org.road.roadd.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
